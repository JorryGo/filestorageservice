<style>
    table {
        border-collapse: collapse;
        margin: 0 auto;
    }

    table, tr, td {
        border: 1px solid gray;
    }

    .content {
        margin: 0 auto;
    }
</style>

<div class="content">
    <table>
        <thead>
        <tr>
            <?php foreach($keys as $key): ?>
                <td><?=$key?></td>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $item): ?>
            <tr>
                <?php foreach ($item as $row): ?>
                    <td><?=$row?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
