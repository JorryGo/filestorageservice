<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStorageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storage', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('storage');
            $table->integer('company_id')->nullable()->index();
            $table->integer('client_id')->nullable()->index();
            $table->integer('ticket_id')->nullable();
            $table->integer('comment_id')->nullable();
            $table->integer('macros_id')->nullable()->index();
            $table->integer('channel_id')->nullable();
            $table->integer('channel_type')->nullable();
            $table->string('type')->comment('mime type of file. May be image/jpeg, application/pdf and others');
            $table->string('path');
            $table->string('filename');
            $table->string('extension');
            $table->jsonb('preview')->nullable();
            $table->smallInteger('status')->default(0);
            $table->timestamps();

            $table->index(['company_id', 'client_id', 'ticket_id']);
            $table->index(['company_id', 'client_id', 'ticket_id', 'comment_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storage');
    }
}
