# Медиамодуль

__Заметка о загрузке файлов__

Файлы могут быть переданы в любом виде, в массиве любой вложенности, они будут адекватно обработаны 

# Запросы
POST `/v1/push` - загрузка файла

Принимаемые параметры:
```
company_id - обязательный
client_id - обязательный
ticket_id - обязательный
comment_id - не обязательный
channel_id - не обязательный
channel_type - не обязательный
```

GET `/v1/get` - получение списка файлов

Принимаемые параметры:
```
company_id - обязательный
Все параметры ниже не обызательны
client_id
ticket_id
comment_id 
channel_id 
channel_type
limit
offset
```

__Загрузка файла для макроса:__

POST `/v1/macros/push/{macros_id}`

Получение файлов для макроса:

GET `/v1/macros/{macros_id}`


__Генерация файлов__

POST `/generate-doc/{type}`

Данные должны быть в json.
Ожидается, что данные придут в теле запроса или в переменной data


# Код

__Запросы__

Для удобства, при необходимости запросы обрабатываются через RequestParams 

Доступ к данным происходит в виде 
```php
public function action(RequestParams $params)
{
    $params->required(['company_id', 'ticket_id']);
    echo $params->company_id;
}
```

Через метод `required()` можно указать обязательные параметры. 
В случае если они не будут переданы сработает исключение `RequiredParamNotExistsException`

__Загрузка файлов__

Загрузка файлов происходит в `PushController` 

Файлы для тикета загружаются в экшне `push`

Для загрузки файлов создается экхемпляр хранилища. 
При его создании можно указать id хранилища. По умолчанию создается локальное (и реализовано пока только локальное)

В зависимости от выбранного хранилища создается экземпляр класса соответствующего хранилища.

Классы хранилищ находятся в `App\Library\StorageWorkers`

К каждому классу хранилища должн быть подключен интерфейс `App\Library\Interfaces\StorageWorkerInterface`

Загрузка/получение файла происходит через экземпляр класса хранилища, запись информации о 
загруженном файле записывается в экшене. 

Для сохранения информации о файлах используется модель 
`App\StorageModel`

__Получение файлов__

Получение файлов происходит в контроллере `GetController`

Экшн `get` служит для получения списка файлов для компании/тикета/комментария

Обязательным является только параметр `company_id`
В зависимости он наличия других параметров происходит выборка по файлам и отдается 
json строка с данными о файлах. 

Ответ формируется через `App\Library\StorageAnswer`

Ссылка на файл выглядит следующим образом: `/load/{id}.{extension}` - `/load/10.jpg`

Расширение указывается только для адекватной обработки браузерами и ни на что не влияет

По id мы получаем запись из базы и подключаем соответствующее хранилище. 
И уже хранилище отдает файл, который мы отправляем пользователю указав правильный mime тип.

# Cоздание превью

При загрузке файла создается задача `App\Jobs\CreatePreviewJob`

На вход принимается модель `App\StorageModel` с файлом 

В зависимости от mime типа создается соответствующий экземпляр класса-создателя превью

Они находятся в `App\Jobs\PreviewGenerators` и к ним должен подключаться интерфейс 
`App\Library\Interfaces\PreviewGeneratorInterface`

В результате работы будут созданы новые файлы и загружены в хранилище основного файла. 
Так же у файла пути к превью будут записаны в колонку preview
