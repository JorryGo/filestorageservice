<?php

namespace App\Jobs;

use App\Library\Interfaces\PreviewGeneratorInterface;
use App\StorageModel;

class CreatePreviewJob extends Job
{
    /**
     * @var StorageModel $file
     */

    private $file;

    private $workers = [
        'image/jpeg' => 'App\Jobs\PreviewGenerators\ImgGenerator',
        'image/png' => 'App\Jobs\PreviewGenerators\ImgGenerator',
    ];

    /**
     * Create a new job instance.
     *
     * @param StorageModel $file
     * @return void
     */
    public function __construct(StorageModel $file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!empty($this->workers[$this->file->type])) {
            $class = $this->workers[$this->file->type];
            return (new $class)->run($this->file);
        }
    }


}
