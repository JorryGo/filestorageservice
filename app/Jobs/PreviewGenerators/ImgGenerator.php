<?php
namespace App\Jobs\PreviewGenerators;

use App\Library\Interfaces\PreviewGeneratorInterface;
use App\Library\Interfaces\StorageWorkerInterface;
use App\Library\Storage;
use App\StorageModel;
use http\Url;
use Spatie\Glide\GlideImage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImgGenerator implements PreviewGeneratorInterface {
    /**
     * generatong previews for images
     */

    public function run(StorageModel $model)
    {
        $storage = new Storage($model->storage);

        $file = $storage->getFileResource($model);

        $tmp_file = $model->id . '.tmp.' . $model->extension;
        file_put_contents($tmp_file, $file);

        GlideImage::create($tmp_file)
            ->modify(['w'=> 50])
            ->save('small.' . $model->extension);

        GlideImage::create($tmp_file)
            ->modify(['w'=> 200])
            ->save('medium.' . $model->extension);


        $small = $this->savePreview($model, 'small.' . $model->extension);
        $medium = $this->savePreview($model, 'medium.' . $model->extension);

        $model->preview = json_encode([
            'small' => [
                'id' => $small->id,
                'path' => str_replace('//:', '//' . env('APP_URL', ''), route('load', ['id' => $small->id . '.' . $small->extension], false)),
            ],
            'medium' => [
                'id' => $medium->id,
                'path' => str_replace('//:', '//' . env('APP_URL', ''), route('load', ['id' => $medium->id . '.' . $medium->extension], false)),
            ]
        ]);

        unlink($tmp_file);

        return $model->save() ? true : false;
    }

    /**
     * @param StorageModel $model
     * @param string $file
     * @return StorageModel
     */
    private function savePreview(StorageModel $model, string $file) :StorageModel
    {
        /**
         * @var StorageWorkerInterface $storage
         */
        $storage = new Storage($model->storage);
        $file = new UploadedFile($file, str_random() . $model->filename, null, null, null, true);
        $path = $storage->save($file);

        $model = StorageModel::create([
            'storage' => $model->storage,
            'company_id' => $model->company_id,
            'client_id' => $model->client_id,
            'ticket_id' => $model->ticket_id,
            'type' => $model->type,
            'path' => $path,
            'filename' => 'preview-' . $model->filename,
            'extension' => $model->extension,
            'status' => StorageModel::STATUS_HIDE,
        ]);

        return $model;
    }

}