<?php
namespace App\Library;

use App\Exceptions\UnknownStorageException;
use App\Exceptions\UnknownStorageMethodException;
use App\Library\Interfaces\StorageWorkerInterface;
use App\Library\StorageWorkers\LocalStorageWorker;
use Mockery\Exception;

class Storage {
    /**
     * Class for switching worker for file
     */

    public $storage_id;

    private $storage;

    const LOCAL_STORAGE = 1;

    public $storages = [
        self::LOCAL_STORAGE => 'App\Library\StorageWorkers\LocalStorageWorker',
    ];

    /**
     * Storage constructor.
     * @param $id
     * @throws UnknownStorageException
     */
    public function __construct($id = self::LOCAL_STORAGE)
    {
        $this->storage = $this->getStorage($id);

        $this->storage_id = $id;
    }

    /**
     * @param $name
     * @param $arguments
     * @return StorageWorkerInterface
     * @throws UnknownStorageMethodException
     */
    public function __call($name, $arguments)
    {
        if (method_exists($this->storage, $name)) {
            return call_user_func_array([$this->storage, $name], $arguments);
        }

        throw new UnknownStorageMethodException();
    }

    /**
     * @param $id
     * @return mixed
     * @throws UnknownStorageException
     */
    private function getStorage($id)
    {
        if (!empty($this->storages[$id])) {
            return new $this->storages[$id];
        }

        throw new UnknownStorageException();
    }
}