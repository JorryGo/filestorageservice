<?php
namespace App\Library;

use App\Exceptions\RequiredParamNotExistsException;
use Illuminate\Http\Request;

class RequestParams {

    /**
     * Load params from request
     * @var integer $company_id
     * @var integer $client_id
     * @var integer $ticket_id
     * @var object $body
     * @var \Symfony\Component\HttpFoundation\FileBag $files
     */

    public $company_id;
    public $client_id;
    public $ticket_id;
    public $comment_id;
    public $channel_id;
    public $channel_type;
    public $macros_id;
    public $limit;
    public $offset;

    public $data;

    public $body;
    public $files;

    /**
     * RequestParams constructor.
     * @param Request $request
     * @throws RequiredParamNotExistsException
     */
    public function __construct(Request $request)
    {
        $this->company_id = $request->input('company_id');
        $this->client_id = $request->input('client_id');
        $this->ticket_id = $request->input('ticket_id');
        $this->comment_id = $request->input('comment_id');
        $this->channel_id = $request->input('channel_id');
        $this->channel_type = $request->input('channel_type');
        $this->macros_id = $request->input('macros_id');
        $this->limit = $request->input('limit');
        $this->offset = $request->input('offset');

        $this->data = $request->input('data');

        if ($request->isMethod('post')) {
            $this->body = json_decode($request->getContent(), true);
        }

        $this->files = $request->files;
    }

    /**
     * @param array $items
     * @throws RequiredParamNotExistsException
     */
    public function required(array $items)
    {
        foreach ($items as $item) {
            if (!$this->$item) {
                throw new RequiredParamNotExistsException($item . ' is required');
            }
        }
    }

}