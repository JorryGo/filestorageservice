<?php
namespace App\Library\DocGenerators;

use App\Library\Interfaces\DocGeneratorInterface;

class Csv extends Base implements DocGeneratorInterface {

    private $mime = 'text/csv';
    private $delimeter = ';';

    public function get()
    {
        return $this->generate();
    }

    private function generate()
    {
        //max file size - 500mb
        $csv = fopen('php://temp/maxmemory:'. (500*1024*1024), 'r+');

        $keys = array_keys($this->data);

        fputcsv($csv, $keys, $this->delimeter);

        foreach ($this->data as $item) {
            fputcsv($csv, $item, $this->delimeter);
        }

        return $csv;
    }

    public function getMime(): string
    {
        return $this->mime;
    }

    public function getFileName(): string
    {
        return date("Y-m-d_H:i:s") . '.csv';
    }

    public function isSelfReturn(): bool
    {
        return false;
    }
}