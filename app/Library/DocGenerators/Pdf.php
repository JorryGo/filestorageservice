<?php
namespace App\Library\DocGenerators;

use App\Library\Interfaces\DocGeneratorInterface;
use Illuminate\Support\Facades\App;

class Pdf extends Base implements DocGeneratorInterface {

    private $mime = 'application/pdf';

    /**
     * @return mixed
     */
    public function get()
    {
        $pdf = App::make('dompdf.wrapper');
        $keys = array_keys($this->data);

        $pdf->loadHTML(view('doc.pdf', [
            'keys' => $keys,
            'data' => $this->data,
        ]));
        return $pdf->stream();
    }

    /**
     * @return string
     */
    public function getMime(): string
    {
        return $this->mime;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return date("Y-m-d_H:i:s") . '.pdf';
    }

    /**
     * @return bool
     */
    public function isSelfReturn(): bool
    {
        return true;
    }

}