<?php
namespace App\Library\DocGenerators;

class Base {

    public $data;

    /**
     * @param $data
     * @return bool
     */
    public function load(array $data) : bool
    {
        if (empty($data)) {
            return false;
        }

        $this->data = $data;

        return true;
    }
}