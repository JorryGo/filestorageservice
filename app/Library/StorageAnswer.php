<?php
namespace App\Library;

use App\StorageModel;

class StorageAnswer {
    /**
     * Class for generating answers from api
     */

    /**
     * @param $models
     * @return array
     */
    public static function byModels($models) :array
    {
        $result = [];

        foreach ($models as $model) {
            $result[] = [
                'id' => $model->id,
                'company_id' => $model->company_id,
                'client_id' => $model->client_id,
                'ticket_id' => $model->ticket_id,
                'comment_id' => $model->comment_id,
                'channel_id' => $model->channel_id,
                'channel_type' => $model->channel_type,
                'type' => $model->type,
                'extension' => $model->extension,
                'path' => route('load', ['id' => $model->id . '.' . $model->extension]),
                'preview' => json_decode($model->preview, true),
            ];
        }

        return $result;
    }

    /**
     * @param $models
     * @return array
     */
    public static function byModelsMacros($models) :array
    {
        $result = [];

        foreach ($models as $model) {
            $result[] = [
                'id' => $model->id,
                'macros_id' => $model->macros_id,
                'type' => $model->type,
                'extension' => $model->extension,
                'path' => route('load', ['id' => $model->id . '.' . $model->extension]),
                'preview' => json_decode($model->preview, true),
            ];
        }

        return $result;
    }

}