<?php
namespace App\Library\StorageWorkers;

use App\Library\Interfaces\StorageWorkerInterface;
use App\StorageModel;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LocalStorageWorker implements StorageWorkerInterface {

    private $storage_path;

    /**
     * LocalStorageWorker constructor.
     */
    public function __construct()
    {
        $path = env('LOCAL_STORAGE_PATH', 'storage/app');
        $this->storage_path = base_path($path);
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function save(UploadedFile $file) :string
    {
        $filename = md5($file->getBasename() . $file->getClientOriginalName());
        $filename .= '.' . $file->getClientOriginalExtension();

        $upload_dir = $this->getUploadDir($filename);
        $full_path = $upload_dir . $filename;

        if (!file_exists($this->storage_path . $upload_dir)) {
            mkdir($this->storage_path . $upload_dir, 666, true);
        }

        $file->move($this->storage_path . $upload_dir, $filename);

        return $full_path;
    }

    /**
     * @param StorageModel $model
     */
    public function get(StorageModel $model)
    {
        $file_path = $this->storage_path . $model->path;

        if (!file_exists($file_path)) {
            throw new NotFoundHttpException('file not found on storage');
        }

        if (ob_get_level()) {
            ob_end_clean();
        }

        header('Content-Description: File Transfer');
        header('Content-Type: ' . $model->type);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file_path));
        readfile($file_path);
    }


    /**
     * @param StorageModel $model
     */
    public function getFileResource(StorageModel $model)
    {
        $file_path = $this->storage_path . $model->path;
        return file_get_contents($file_path);
    }

    /**
     * @param string $filename
     * @return string
     */
    private function getUploadDir(string $filename) :string
    {
        $first = mb_substr($filename, 0, 3);
        $two = mb_substr($filename, 3, 3);

        $path = '/' . $first . '/' . $two . '/';

        return $path;
    }

}