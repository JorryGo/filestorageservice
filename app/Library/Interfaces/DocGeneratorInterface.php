<?php
namespace App\Library\Interfaces;

interface DocGeneratorInterface {

    public function get();

    public function getMime() : string;

    public function getFileName() : string;

    public function isSelfReturn() : bool;

}