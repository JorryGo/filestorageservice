<?php
namespace App\Library\Interfaces;

use App\StorageModel;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface StorageWorkerInterface for implementing storage workers
 * @package App\Library\Interfaces
 */
interface StorageWorkerInterface {

    public function save(UploadedFile $file);

    public function get(StorageModel $model);
}