<?php
namespace App\Library\Interfaces;

use App\StorageModel;

interface PreviewGeneratorInterface {

    public function run(StorageModel $model);

}