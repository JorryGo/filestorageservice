<?php

namespace App\Http\Controllers;

use App\Events\FileLoadEvent;
use App\Exceptions\FileIsRequiredException;
use App\Exceptions\RequiredParamNotExistsException;
use App\Helpers\ArrayHelper;
use App\Jobs\CreatePreviewJob;
use App\Library\Interfaces\StorageWorkerInterface;
use App\Library\Storage;
use App\Library\RequestParams;
use App\Library\StorageAnswer;
use App\StorageModel;
use Illuminate\Support\Facades\Cache;

class PushController extends Controller
{

    /**
     * File loader action
     * @param RequestParams $params
     * @return array
     * @throws FileIsRequiredException
     * @throws RequiredParamNotExistsException
     */
    public function push(RequestParams $params)
    {
        /**
         * @var \Symfony\Component\HttpFoundation\File\UploadedFile[] $files
         * @var StorageWorkerInterface $storage
         */

        $params->required(['company_id', 'client_id', 'ticket_id']);

        $files = ArrayHelper::toSingle($params->files->all());

        if (!$files) {
            throw new FileIsRequiredException('Files is empty');
        }

        $storage = new Storage();

        $models = [];

        foreach ($files as $file) {
            $type = $file->getMimeType();
            $real_filename = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $path = $storage->save($file);

            $models[] = $file = StorageModel::create([
                'storage' => $storage->storage_id,
                'company_id' => $params->company_id,
                'client_id' => $params->client_id,
                'ticket_id' => $params->ticket_id,
                'comment_id' => $params->comment_id,
                'channel_id' => $params->channel_id,
                'channel_type' => $params->channel_type,
                'type' => $type,
                'path' => $path,
                'filename' => $real_filename,
                'extension' => $ext,
            ]);

            dispatch(new CreatePreviewJob($file));
        }

        return [
            'result' => StorageAnswer::byModels($models)
        ];
    }

    /**
     * Uploading files for macros
     * @param $id
     * @param RequestParams $params
     * @return array
     * @throws FileIsRequiredException
     */
    public function macrosPush(int $id, RequestParams $params) {
        /**
         * @var \Symfony\Component\HttpFoundation\File\UploadedFile[] $files
         * @var StorageWorkerInterface $storage
         */

        $files = ArrayHelper::toSingle($params->files->all());

        if (!$files) {
            throw new FileIsRequiredException('Files is empty');
        }

        $storage = new Storage();

        $models = [];

        foreach ($files as $file) {
            $type = $file->getMimeType();
            $real_filename = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $path = $storage->save($file);

            $models[] = $file = StorageModel::create([
                'storage' => $storage->storage_id,
                'macros_id' => $id,
                'type' => $type,
                'path' => $path,
                'filename' => $real_filename,
                'extension' => $ext,
            ]);

            dispatch(new CreatePreviewJob($file));
        }

        return [
            'result' => StorageAnswer::byModelsMacros($models)
        ];
    }
}
