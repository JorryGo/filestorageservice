<?php
namespace App\Http\Controllers;

use App\Library\Interfaces\StorageWorkerInterface;
use App\Library\RequestParams;
use App\Library\Storage;
use App\Library\StorageAnswer;
use App\StorageModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Cache;

class GetController extends Controller {
    /**
     * Class for getting files from storage
     */

    /**
     * @param string $id
     * @return mixed
     */
    public function load(string $id)
    {
        /**
         * @var StorageWorkerInterface $storage
         */

        [$id, $ext] = explode('.', $id);
        $cache_key = 'file:' . $id;

        if (!$model = Cache::get($cache_key)) {
            try {
                $model = StorageModel::findOrFail($id);
                Cache::put($cache_key, $model, env('CACHE_LEAVE', 10));
            } catch (ModelNotFoundException $e) {
                abort(404);
            }
        }

        $storage = new Storage($model->storage);
        return $storage->get($model);
    }

    /**
     * @param RequestParams $params
     * @return array
     * @throws \App\Exceptions\RequiredParamNotExistsException
     */
    public function get(RequestParams $params)
    {
        $params->required(['company_id']);
        $query = StorageModel::where('company_id', $params->company_id)
            ->where('status', StorageModel::STATUS_SHOW);

        $filteringParams = ['client_id', 'ticket_id', 'comment_id', 'channel_id', 'channel_type'];

        foreach ($filteringParams as $filteringParam) {
            if ($params->$filteringParam) {
                $query->where($filteringParam, $params->$filteringParam);
            }
        }

        $total = $query->count();

        if ($params->limit and $params->limit <= 100) {
            $query->limit($params->limit);
        } else {
            $query->limit(100);
        }

        $query->offset((int)$params->offset);

        return [
            'total' => $total,
            'result' => StorageAnswer::byModels($query->get())
        ];
    }

    public function macrosGet(int $id) {

        $files = StorageModel::where('macros_id', $id)->get();

        return [
            'total' => count($files),
            'result' => StorageAnswer::byModelsMacros($files),
        ];
    }
}