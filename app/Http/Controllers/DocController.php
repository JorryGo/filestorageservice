<?php
namespace App\Http\Controllers;

use App\Exceptions\RequiredParamNotExistsException;
use App\Exceptions\UnknownDocGeneratorException;
use App\Http\Controllers\Controller;
use App\Library\Interfaces\DocGeneratorInterface;
use App\Library\RequestParams;

class DocController extends Controller {
    /**
     * Generate documents
     */

    private $docGeneratorPath = 'App\Library\DocGenerators\\';

    /**
     * @param $type
     * @param RequestParams $params
     * @throws UnknownDocGeneratorException
     * @throws RequiredParamNotExistsException
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function get(string $type, RequestParams $params)
    {
        /**
         * @var DocGeneratorInterface $generator
         */
        $type = ucfirst($type);
        $className = $this->docGeneratorPath . $type;
        $data = $params->body ?? json_decode($params->data, true);

        if (!class_exists($className)) {
            throw new UnknownDocGeneratorException();
        }

        $generator = new $className;

        if (!$generator->load($data)) {
            throw new RequiredParamNotExistsException('data should be as json in request body');
        }

        if ($generator->isSelfReturn()) {
            return $generator->get();
        }

        $result = $generator->get();

        header('Content-Description: File Transfer');
        header('Content-Type: ' . $generator->getMime());
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Content-Disposition: attachment; filename="' . $generator->getFileName() . '"');
        header('Content-Length: ' . fstat($result)['size']);

        fseek($result, 0);
        fpassthru($result);
    }

}