<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StorageModel
 * @package App
 */
class StorageModel extends Model
{

    /**
     * Storage model
     * @property int $id
     * @property int $storage
     * @property int $company_id
     * @property int $client_id
     * @property int $ticket_id
     * @property int $comment_id
     * @property int $channel_id
     * @property int $channel_type
     * @property string $type
     * @property string $path
     * @property string $filename
     * @property string $extension
     * @property string $preview
     * @property string $status
     * @property string $created_at
     * @property string $updated_at
     */

    const QUEUE_TYPE_PREVIEW_CREATE = 1;
    const QUEUE_TYPE_PREVIEW_DELETE = 2;

    const STATUS_SHOW = 0;
    const STATUS_HIDE = 1;

    protected $table = 'storage';

    public $timestamps = true;

    protected $fillable = [
        'storage',
        'company_id',
        'client_id',
        'ticket_id',
        'comment_id',
        'macros_id',
        'channel_id',
        'channel_type',
        'type',
        'path',
        'filename',
        'extension',
        'status',
    ];

    /**
     * Set queue for creating previews. Deleting previews after delete
     */
    public static function boot()
    {
        parent::boot();

        self::created(function(StorageModel $model){
           $model->previewsQueue(self::QUEUE_TYPE_PREVIEW_CREATE);
        });

        self::updated(function(StorageModel $model){
            $model->previewsQueue(self::QUEUE_TYPE_PREVIEW_CREATE);
        });

        self::deleted(function(StorageModel $model){
            $model->previewsQueue(self::QUEUE_TYPE_PREVIEW_DELETE);
        });
    }

    /**
     * Creating queue task for previews
     * @param $type
     */
    public function previewsQueue(int $type)
    {
        
    }
    
}