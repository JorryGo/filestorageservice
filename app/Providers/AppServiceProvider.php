<?php

namespace App\Providers;

use App\Library\RequestParams;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     * @return void
     */
    public function register()
    {
        $this->app->singleton(RequestParams::class, function ($app) {
            return new RequestParams($this->app->request);
        });
    }
}
