<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{

    const DEFAULT_ERROR_STATUS_CODE = 500;

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return mixed
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof RequiredParamNotExistsException) {
            return response([
                'error' => $e->getMessage()
            ], self::DEFAULT_ERROR_STATUS_CODE);
        }

        if ($e instanceof MethodNotAllowedHttpException) {
            return response([
                'error' => 'wrong method'
            ], self::DEFAULT_ERROR_STATUS_CODE);
        }

        if ($e instanceof FileIsRequiredException) {
            return response([
                'error' => $e->getMessage()
            ], self::DEFAULT_ERROR_STATUS_CODE);
        }

        if ($e instanceof UnknownDocGeneratorException) {
            return response([
                'error' => 'Unknown output format'
            ], self::DEFAULT_ERROR_STATUS_CODE);
        }

        if ($e instanceof NotFoundHttpException) {
            return parent::render($request, $e);
        }

        $default_response = env('APP_DEBUG', false)
            ?
                get_class($e) . ': ' . $e->getMessage()
            :
                'server error';

        return response([
            'error' => $default_response
        ], self::DEFAULT_ERROR_STATUS_CODE);
    }
}
