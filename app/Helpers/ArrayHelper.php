<?php
namespace App\Helpers;

class ArrayHelper {

    /**
     * Class with static methods for working with arrays
     */

    /**
     * Convert multi array to single array
     * @param array $array
     * @return array
     */
    public static function toSingle(array $array) : array
    {
        $single = [];

        foreach ($array as $item) {
            if (is_array($item)) {
                $single = array_merge($single, self::toSingle($item));
                continue;
            }

            $single[] = $item;
        }

        return $single;
    }

}