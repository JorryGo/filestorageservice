<?php
/** test
 * @var \Laravel\Lumen\Routing\Router $router
 */


$router->post('/push', [
    'as' => 'push',
    'uses' => 'PushController@push',
]);

$router->get('/load/{id}', [
    'as' => 'load',
    'uses' => 'GetController@load',
]);

$router->get('/get', [
    'as' => 'get',
    'uses' => 'GetController@get',
]);

$router->group(['prefix' => 'macros'], function(\Laravel\Lumen\Routing\Router $router){
    $router->post('/push/{id}', [
        'as' => 'macros.push',
        'uses' => 'PushController@macrosPush',
    ]);
    $router->get('/get/{id}', [
        'as' => 'macros.get',
        'uses' => 'GetController@macrosGet',
    ]);
});

$router->post('/generate-doc/{type}', [
    'as' => 'doc.generate',
    'uses' => 'DocController@get',
]);